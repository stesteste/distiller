# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem

import db.models

class DbExist(object):
    def process_item(self, item, spider):
        # if db.models.Company.objects.filter(name=item['name']):
        #     raise DropItem('gia presente: "%s"')
        return item


class DbSave(object):
    def process_item(self, item, spider):
        if not db.models.Company.objects.filter(name=item['name']):
            db.models.Company(**item).save()
