import scrapy

from negozi.items import NegoziItem

def xpath_helper(row, selector):
    try:
        return row.xpath(selector).extract()[0].strip()
    except:
        return ''


class NegoziSpider(scrapy.Spider):
    name = "pagine_gialle_negozi"
#    allowed_domains = "www.paginegialle.it"
    start_urls = ['http://www.paginegialle.it/ricerca/negozi/Settimo%20Torinese%20%28TO%29', ]

    def parse(self, response):
        for row in response.xpath('//div[@class="vcard listElement"]'):
            item = NegoziItem()
            item['name'] = xpath_helper(row, './/span[@class="fn elementTitle"]/a/text()').title()
            item['street_address'] = xpath_helper(row, './/span[@class="street-address"]/text()')
            item['postal_code'] = xpath_helper(row, './/span[@class="postal-code"]/text()')
            if not item['postal_code']:
                item['postal_code'] = 0
            item['locality'] = xpath_helper(row, './/span[@class="locality"]/text()')
            item['region'] = xpath_helper(row, './/span[@class="region"]/text()')
            item['latitude'] = xpath_helper(row, './/span[@itemprop="latitude"]/text()')
            if not item['latitude']:
                item['latitude'] = 0.0
            item['longitude'] = xpath_helper(row, './/span[@itemprop="longitude"]/text()')
            if not item['longitude']:
                item['longitude'] = 0.0
            item['tel'] = xpath_helper(row, './/span[@class="hidetel_rtg"]/text()')
            item['web'] = xpath_helper(row, './/a[@class="dropdownBtn"]/@href')
            yield item
        for href in response.xpath('//a[@class="paginationBtn arrowBtn rightArrowBtn"]/@href').extract():
            yield scrapy.Request(href, callback=self.parse)
