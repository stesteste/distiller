from django.core.management.base import BaseCommand
from django.db import connection

import time

class Command(BaseCommand):
    help = "Dump table"

    def handle(self, *args, **options):
        curs = connection.cursor()
        curs.execute('show tables')
        tabelle_interessanti = [tabella[0] for tabella in curs.fetchall()
                            if (not tabella[0].startswith('auth_')) and
                            (not tabella[0].startswith('django'))]
        for tabella in tabelle_interessanti:
            timer_start = time.time()
            curs.execute('select count(*) from %s;'%tabella)
            print '%s: %d row [time: %.4f sec]'%(tabella, curs.fetchall()[0][0], time.time()-timer_start)
