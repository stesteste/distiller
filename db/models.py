from django.db import models


# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=1024)
    street_address = models.CharField(max_length=1024)
    postal_code = models.IntegerField()
    locality = models.CharField(max_length=1024)
    region = models.CharField(max_length=1024)
    latitude = models.FloatField()
    longitude = models.FloatField()
    tel = models.CharField(max_length=1024)
    web = models.URLField(max_length=1024)

    def __str__(self):
        return self.name.title()
