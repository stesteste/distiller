from django.contrib import admin

import models


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'tel', 'web')


# Register your models here.
admin.site.register(models.Company,CompanyAdmin)
