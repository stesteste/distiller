# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1024)),
                ('street_address', models.CharField(max_length=1024)),
                ('postal_code', models.IntegerField()),
                ('locality', models.CharField(max_length=1024)),
                ('region', models.CharField(max_length=1024)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('tel', models.CharField(max_length=1024)),
                ('web', models.URLField(max_length=1024)),
            ],
        ),
    ]
